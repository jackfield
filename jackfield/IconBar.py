# Manages the icon bar for Jackfield
# Look through each of the home dirs for widgets
# For each thing in those dirs, call each widget engine to see if
#  it owns it. If it does, store away (widgetpath, widgetengine)
#  ready for when someone tries to start that widget later

import os, sys

WIDGETDIRS = [
  os.path.expanduser("~/Library/Jackfield/Widgets/"),
  os.path.expanduser("~/.jackfield/Widgets/"),
  os.path.expanduser("~/Projects/jackfield/Widgets"), # sil test dir!
  "/usr/share/jackfield/Widgets"
]

import jackfield

class IconBar:
  def __init__(self, basedir):
    self.basedir = basedir
    self.widgets = {}
    self.import_widget_engines()
    
  def import_widget_engines(self):
    self.widgetengines = []
    MODULES_LIST = os.listdir(os.path.join(self.basedir,"WidgetEngines"))
    for mname in MODULES_LIST:
      if mname.startswith("."): continue
      sys.path.append(os.path.join(self.basedir,"WidgetEngines", mname))
      self.widgetengines.append(__import__(mname, globals(), locals(), []))
  
  def get_changed_widgets(self):
    """Work out any widgets which weren't here the last time we called this.
       This means that when we start up they'll *all* be new."""
    
    # Mark all the widgets we know about as unchecked, so we can know
    # later whether any have been deleted
    for fn in self.widgets.keys():
      self.widgets[fn]["checked"] = False   
    
    new_widgets = []
    
    for wdir in WIDGETDIRS:
      if not os.path.isdir(wdir): continue
      for f in os.listdir(wdir):
        fn = os.path.join(wdir, f)
        if self.widgets.has_key(fn):
          self.widgets[fn]["checked"] = True
          continue # this widget's already in our list
        for engine in self.widgetengines:
          data = engine.check_widget(fn)
          if data:
            self.widgets[fn] = {
              "path": fn,
              "engine": engine,
              "icon": data["icon"],
              "checked": True
            }
            new_widgets.append(self.widgets[fn])
    # We've now checked all the widgets. Go through the list, and any
    # that are checked:False have been deleted since the last time we
    # did this.
    deleted_widgets = []
    for fn in self.widgets.keys():
      if not self.widgets[fn]["checked"]:
        deleted_widgets.append(self.widgets[fn])
        del(self.widgets[fn])
        
    return (new_widgets, deleted_widgets)
    
  def get_widget_engine(self, path):
    return self.widgets[path]["engine"]
