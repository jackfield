import tempfile, shutil, os, mimetypes, random, string, sys, re

class WidgetBase:
  """Base class for widget implementations.
     Subclasses are expected to override the following functions:
       fix_html
       inject_compatibility_hacks
       inject_widget_object
       inject_extra
       
     They are not expected to override the others, although
     they can if they need to.
     """
  def __init__(self, original_widget_path, root_temporary_dir,
               javascript_extras_dir, iframe_name=None,
               xposition=-1, yposition=-1):
    # make sure that widgetpath doesn't end with a /
    self.original_widget_path = original_widget_path.strip()
    if self.original_widget_path.endswith('/'):
      self.original_widget_path = self.original_widget_path[:-1]
    self.root_temporary_dir = root_temporary_dir
    self.javascript_extras_dir = javascript_extras_dir
    self.initial_file = "testwidget.html"
    self.INJECTED_CODE = ""
    if iframe_name:
      self.iframe_name = iframe_name
    else:
      self.iframe_name = ''.join([random.choice(string.letters) for i in xrange(10)])
    self.initial_xposition = xposition
    self.initial_yposition = yposition
    
  def run(self, callback_function):
    # don't override this function.
    self.copy_to_temp_dir()
    self.calculate_html_files()
    self.fix_html()
    # Inject lots of bits of code into various files in the widget.
    # If the function is injecting the same code into all HTML files
    # in the widget, then don't do the injection yourself; add the
    # code to self.INJECTED_CODE and then it gets done for you.
    # If you need to inject code into files other than HTML files,
    # or different code depending on which file it is, then you
    # have to do the injection yourself.
    self.inject_jackfield_object()
    self.inject_iframe_name()
    self.inject_resize_code()
    self.inject_drag_code()
    self.inject_compatibility_hacks()
    self.inject_widget_object()
    self.inject_extra()
    self.do_injections()
    callback_function(self.original_widget_path,
                      os.path.join(self.widget_path,self.initial_file), 
                      self.iframe_name,
                      self.initial_xposition, self.initial_yposition)

  def copy_to_temp_dir(self):
    "Create a temporary copy of the widget in the system temp dir"  
    widgetname = os.path.split(self.original_widget_path)[1]
    tdir = tempfile.mkdtemp(prefix=widgetname,dir=self.root_temporary_dir)
    self.widget_path = os.path.join(tdir,'widget')
    shutil.copytree(self.original_widget_path,
                    self.widget_path)
  
  def calculate_html_files(self):
    """An optimisation: lots of things need to know the list of all
       HTML files in the widget, so work them out once."""
    self.HTML_FILES = []
    for root, dirs, files in os.walk(self.widget_path):
      self.HTML_FILES += [os.path.join(root,x) for x in files 
             if mimetypes.guess_type(os.path.join(root,x))[0] == "text/html"]
                      
  def inject_jackfield_object(self):
    self.INJECTED_CODE += (
               '<script src="file://%s/jackfield_jackfieldobject.js">' +
               '</script>\n') % self.javascript_extras_dir

  def inject_iframe_name(self):
    self.INJECTED_CODE += ('<script type="text/javascript">\n' +
                           "Jackfield.iframe_name = '%s'\n" + 
                           '</script>\n') % self.iframe_name
  def inject_resize_code(self):
    self.INJECTED_CODE += ('<script src="file://%s/jackfield_resize.js">' +
               '</script>\n') % self.javascript_extras_dir
  def inject_drag_code(self):
    self.INJECTED_CODE += ('<script src="file://%s/jackfield_drag.js">' +
               '</script>\n') % self.javascript_extras_dir

  def fix_html(self):
    # to be overridden to make changes to the temporary HTML
    # that can't be done after-the-fact from JavaScript
    pass
  def inject_compatibility_hacks(self):
    # to be overridden for compatibility stuff to make widgets
    # work in Firefox on Linux even if they're not written for it
    pass
  def inject_widget_object(self):
    # to be overridden to provide the widget JavaScript with
    # some magic object that it can call to do stuff
    pass
  def inject_extra(self):
    # to be overridden to do anything extra that a widget needs
    pass

  def do_injections(self):
    """Write self.INJECTED_CODE into each HTML file."""
    for f in self.HTML_FILES:
      fp = open(f)
      data = fp.read()
      INJECTED_CODE_NO_NEWLINE = self.INJECTED_CODE.replace('\n',' ');
      headre = re.compile('<head>',re.I)
      data = headre.sub('<head>'+INJECTED_CODE_NO_NEWLINE,data)
      fp.close()
      fp = open(f,"w")
      fp.write(data)
      fp.close()
  
