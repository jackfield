# /usr/bin/python

THEPLAN = """

Classes: mainwindow, widget

mainwindow:
  __init__:
    creates gtkmozembed
    size to screen
    load base HTML document
    
  addwidget(widgetpath):
    create new Widget object(widgetpath)
    widget.run(callback=displaywidget)
    
  displaywidget(widgetpathintmp):
    add an iframe to stream, src=widgetpathintmp
    
  getdatafromhtml:
    pass function:::param1:::param2:::etc
    call self.function(param1,param2)

widget:
  __init__(widgetpath)
  
  run(callbackfunction):
    start a separate thread so it doesn't block?
    (each of these as separate functions so they can be overridden)
    copy widgetpath to tmp dir
    process files in tmp dir for filename case, <script/>, etc
    inject resize code
    inject drag widget code (alt+drag? just drag?)
    inject compatibility hacks
    inject widget object
    end thread?
    callbackfunction(tmpdir)

base HTML document
  contains:
    iframes
    iconbar
      calls sendtopython(addwidget:::widgetpath) when one is clicked
    sendtopython(param1,param2)
    rebuildiconbar([widgetpath,widgeticon,widgetname],...)
      dumps the existing icon bar structure and recreates it
    background image or colour
    loadwidget(widgetpath)

"""

import gtk
from MainWindow import MainWindow

mw = MainWindow()
gtk.main()
