# Mini wrapper to get a screenshot directly from X
# Depends on the pure-Python Xlib library, which is bundled
from Xlib import X, display
import Image

def getimage(targetwindow, origin_x, origin_y, width=1, height=1):
    AllPlanes = ~0 # from /usr/X11R6/include/Xlib.h
    try:
        ret = targetwindow.get_image(origin_x, origin_y, width, height, X.ZPixmap, AllPlanes)
        if ret == 0: raise GetImageFailed
    except error.BadDrawable:
        raise GetImageFailed
    except error.BadMatch:
        raise GetImageFailed
    except error.BadValue:
        raise GetImageFailed
    return ret

def getPILimage(targetwindow, origin_x, origin_y, width=1, height=1):
    ret = getimage(targetwindow, origin_x, origin_y, width, height)
    retimage = Image.fromstring("RGBX", (width, height), ret.data, "raw", "BGRX")
    return retimage.convert("RGB")

def GetXScreenshot(w,h):
  rootwin = display.Display().screen().root
  return getPILimage(rootwin, 0, 0, w, h)
  
  
