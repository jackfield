import re, os

SLR = re.compile(r'/System/Library/WidgetResources')
SCR = re.compile(r'<script([^>]+)/ *>',re.I)

filesToReplace = []
filenamesToFix = []

def FixHTML(html):
  # Replace /System/Library/WidgetResources with a reference to our WidgetResources
  OWR = os.path.join(os.path.split(__file__)[0],'Resources')
  #if not OWR.startswith('file://'): OWR = 'file://' + OWR
  html = SLR.sub(OWR,html)

  # find any occurrences of the filenames in filesToReplace and
  # replace them with .filename, which is our poked version.
  # Note that it corrects the case of wrongly-cased things because
  # Linux's filesystem is case-sensitive and OS X's isn't.
  for f in filesToReplace:
    r = re.compile(re.escape(f),re.I)
    html = r.sub(f,html)

  # find any occurrences of the filenames in filenamesToFix
  # and correct their case.
  for f in filenamesToFix:
    r = re.compile(re.escape(f),re.I)
    html = r.sub(f,html)
  

  # fix broken script tags: Safari understands <script src="..." /> 
  # and Moz doesn't.
  html = SCR.sub(r'<script\1></script>',html)
  
  return html
