import os
from xml.dom import minidom

def InfoParser(infopath):
  if not os.path.exists(infopath):
    raise "No Info.plist at %s" % infopath
  dom = minidom.parse(infopath)
  l = [x for x 
             in dom.getElementsByTagName('dict')[0].childNodes
             if x.nodeType != 3]
  kv = zip(l[::2], l[1::2])
  retvals = {}
  for k,v in kv:
    key = k.firstChild.nodeValue
    if v.nodeName == 'string':
      try:
        value = v.firstChild.nodeValue
      except:
        value = ''
    elif v.nodeName == 'true':
      value = True
    elif v.nodeName == 'integer':
      try:
        value = int(v.firstChild.nodeValue)
      except:
        value = 0
    else:
      value = ''
    retvals[key] = value
  return retvals
