"""A widget engine for Apple's Dashboard.
Widget engines must implement a class Widget, which subclasses
WidgetBase.WidgetBase, and a function check_widget, which is passed
a path and should check that path to see if it's a widget we own. If
it isn't, return False. If it is, return a dictionary containing keys
"icon".

The goal of check_widget is to return False as quickly as possible if it's
not one of your widgets.

Widgets must set self.initial_file to be the start file!
"""

import os
import WidgetBase
from InfoParser import InfoParser
import FixHTML

import jackfield

def check_widget(widget_path):
  if not os.path.isdir(widget_path): return False
  
  filesindir = [x.lower() for x in os.listdir(widget_path)]
  vitalfiles = ['Info.plist','Icon.png','Default.png']
  if [x for x in vitalfiles if x.lower() not in filesindir]:
    return False # missing one of the vital files
  # case insensitively find Info.plist
  infoplist = [x for x in os.listdir(widget_path) 
               if x.lower() == "info.plist"][0]
  info = InfoParser(os.path.join(widget_path,infoplist))
  if not info.has_key("MainHTML"): return False # no main html file specified
  startfile = os.path.join(widget_path,info["MainHTML"])
  if not os.path.exists(startfile): return False # main html file nonexistent

  # Looks like a Dashboard widget! gather the necessary data.
  return {
    "icon": os.path.join(widget_path,
            [x for x in os.listdir(widget_path) 
             if x.lower() == "icon.png"][0])
  }
  

class Widget(WidgetBase.WidgetBase):
  def __init__(self, original_widget_path, root_temporary_dir,
               javascript_extras_dir, iframe_name=None,
               xposition=-1, yposition=-1):
    WidgetBase.WidgetBase.__init__(self, original_widget_path, 
               root_temporary_dir, javascript_extras_dir, iframe_name,
               xposition, yposition)

    self.RES_DIR =os.path.join(jackfield.DATADIR,jackfield.PACKAGE,'widgets','dashboard')
    
  def inject_compatibility_hacks(self):
    self.INJECTED_CODE += ('<script src="file://%s/safari_compatibility.js">' + 
               '</script>\n') % self.RES_DIR

  def inject_widget_object(self):
    self.INJECTED_CODE += ('<script src="file://%s/widget_object.js">' + 
               '</script>\n') % self.RES_DIR
  
  def inject_extra(self):
    # Inject the resources path
    self.INJECTED_CODE += ('<script type="text/javascript">\n' +
            'Jackfield.ResourcesPath = "file://%s";\n' +
            '</script>') % self.RES_DIR
    # Inject the close box
    try:
      closex = int(self.__info['CloseBoxInsetX'])
    except:
      # The user may not have specified a closeboxinset, or 
      # may have (wrongly) specified a non-integer
      closex = 20
    try:
      closey = int(self.__info['CloseBoxInsetY'])
    except:
      closey = 20   
    self.INJECTED_CODE += (
     '''<script src="file://%s/closebox.js"></script>
        <script type="text/javascript">
        Jackfield.CloseBox.x = %s;
        Jackfield.CloseBox.y = %s;
        window.addEventListener("DOMContentLoaded",Jackfield.CloseBox.init,false);
        </script>''' % (self.RES_DIR, closex, closey))
    
    # Inject custom CSS
    self.INJECTED_CODE += (
     '''<style type="text/css">
        @import url(file://%s/general.css);
        </style>\n''') % self.RES_DIR
    
    # This doesn't actually inject anything, but we get called after all the
    # other processing has happened, so it's a convenient place to
    # store the initial file
    infoplist = [x for x in os.listdir(self.widget_path) 
                 if x.lower() == "info.plist"][0]
    info = InfoParser(os.path.join(self.widget_path,infoplist))
    self.initial_file = os.path.join(self.widget_path,info["MainHTML"])
  
  def fix_html(self):
    # Go through each of the html, css, and JS files, pass their
    # contents through FixHTML, and save them again
    self.fileNamesToChange = []
    self.fileNamesToFix = []
    self.filesToChange = []
    for thisd,subdirs,files in os.walk(self.widget_path):
      self.fileNamesToChange.extend([x for x in files if 
         (x.endswith('.html') or
          x.endswith('.js') or
          x.endswith('.css'))
         and not x.startswith('.')])
      self.fileNamesToFix.extend([x for x in files if 
          not x.endswith('.html') and
          not x.endswith('.js') and
          not x.endswith('.css') and
          not x.startswith('.')])
      for f in files:
        if (f.endswith('.html') or
            f.endswith('.js') or
            f.endswith('.css')) and not f.startswith('.'):
          fileFullPath = os.path.normpath(os.path.join(thisd,f))
          fileShortPath = os.path.normpath(self.widget_path)
          self.filesToChange.append(fileFullPath[len(fileShortPath)+1:])
    
    FixHTML.filesToReplace = self.fileNamesToChange
    FixHTML.filenamesToFix = self.fileNamesToFix
    for f in self.filesToChange:
      ofn = os.path.join(self.widget_path,f)
      fp = open(ofn)
      data = fp.read()
      # fix Mac line endings
      data = data.replace('\r','\n')
      data = FixHTML.FixHTML(data)
      fp.close()
      fp = open(ofn,'w')
      fp.write(data)
      fp.close()

