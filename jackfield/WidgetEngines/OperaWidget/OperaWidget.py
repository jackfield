"""A widget engine for Opera's widgets..
Widget engines must implement a class Widget, which subclasses
WidgetBase.WidgetBase, and a function check_widget, which is passed
a path and should check that path to see if it's a widget we own. If
it isn't, return False. If it is, return a dictionary containing keys
"icon".

The goal of check_widget is to return False as quickly as possible if it's
not one of your widgets.

Widgets must set self.initial_file to be the start file!
"""

import os, zipfile, tempfile
import WidgetBase

import jackfield

def check_widget(widget_path):
  try:
    zipf = zipfile.ZipFile(widget_path, 'r')
  except:
    return False
  
  filesindir = zipf.namelist()
  vitalfiles = ['config.xml', 'index.html']
  if [x for x in vitalfiles if x not in filesindir]:
    return False # missing one of the vital files
  
  MY_DIR = os.path.normpath(
           os.path.join(os.getcwd(),os.path.split(__file__)[0])
           )
  RES_DIR =os.path.join(MY_DIR, 'Resources')
  # Looks like an Opera widget! gather the necessary data.
  return {
    "icon": os.path.join(RES_DIR, "widget_icon.png")
  }
  

class Widget(WidgetBase.WidgetBase):
  def __init__(self, original_widget_path, root_temporary_dir,
               javascript_extras_dir, iframe_name=None,
               xposition=-1, yposition=-1):
    WidgetBase.WidgetBase.__init__(self, original_widget_path, 
               root_temporary_dir, javascript_extras_dir, iframe_name,
               xposition, yposition)

    self.RES_DIR =os.path.join(jackfield.DATADIR,jackfield.PACKAGE,'widgets','opera')
    self.initial_file = "index.html"
    
  def inject_compatibility_hacks(self):
    self.INJECTED_CODE += ('<script src="file://%s/opera_compatibility.js">' + 
               '</script>\n') % self.RES_DIR
  
  def inject_widget_object(self):
    self.INJECTED_CODE += ('<script src="file://%s/widget_object.js">' + 
               '</script>\n') % self.RES_DIR
  
  def inject_extra(self):
    # don't really need closebox, but the main jackfield code calls it
    self.INJECTED_CODE += (
     '''<script src="file://%s/closebox.js"></script>''' % (self.RES_DIR))
    
  def fix_html(self):
    pass
    

  def copy_to_temp_dir(self):
    "Create a temporary copy of the widget in the system temp dir"  
    widgetname = os.path.split(self.original_widget_path)[1]
    tdir = tempfile.mkdtemp(prefix=widgetname,dir=self.root_temporary_dir)
    self.widget_path = os.path.join(tdir,'widget')
    zipf = zipfile.ZipFile(self.original_widget_path, 'r')
    # create folders
    for name in [x for x in zipf.namelist() if x.endswith('/')]:
      ndir = os.path.join(self.widget_path,name)
      os.makedirs(ndir)
    for name in [x for x in zipf.namelist() if not x.endswith('/')]:
      fp = open(os.path.join(self.widget_path,name), 'wb')
      fp.write(zipf.read(name))
      fp.close()

