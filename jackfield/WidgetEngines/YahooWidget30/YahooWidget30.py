"""A widget engine for Yahoo Widgets of versions <= 3 (i.e.,
does not support v4 widgets, which are in some opaque undocumented
binary format which we get to reverse engineer later.
.
Widget engines must implement a class Widget, which subclasses
WidgetBase.WidgetBase, and a function check_widget, which is passed
a path and should check that path to see if it's a widget we own. If
it isn't, return False. If it is, return a dictionary containing keys
"icon".

The goal of check_widget is to return False as quickly as possible if it's
not one of your widgets.

Widgets must set self.initial_file to be the start file!
"""

import os, zipfile, tempfile
import WidgetBase

import jackfield

def check_widget(widget_path):
  # first, is it a folder with a .kon file in?
  if os.path.isdir(widget_path):
    contentsdir = os.path.join(widget_path, 'Contents')
    if not os.path.isdir(contentsdir):
      return False
    # is there a .kon file?
    if not [x for x in os.listdir(contentsdir) if x.endswith('.kon')]:
      return False
  else:
    # is it a zipfile
    try:
     zipf = zipfile.ZipFile(widget_path, 'r')
    except:
      return False
    filesindir = zipf.namelist()
    if not [x for x in filesindir if x.find('/Contents/') != -1
                                 and x.endswith('.kon')]:
      return False # no .kon file
  
    RES_DIR =os.path.join(jackfield.DATADIR,jackfield.PACKAGE,'widgets','yahoo')
  # Looks like a Yahoo widget! gather the necessary data.
  return {
    "icon": os.path.join(RES_DIR, "widget_icon.png")
  }
  

class Widget(WidgetBase.WidgetBase):
  def __init__(self, original_widget_path, root_temporary_dir,
               javascript_extras_dir, iframe_name=None,
               xposition=-1, yposition=-1):
    WidgetBase.WidgetBase.__init__(self, original_widget_path, 
               root_temporary_dir, javascript_extras_dir, iframe_name,
               xposition, yposition)
    #print "CREATING YAHOO WIDGET",original_widget_path
    self.MY_DIR = os.path.normpath(
           os.path.join(os.getcwd(),os.path.split(__file__)[0])
           )
    self.RES_DIR =os.path.join(self.MY_DIR, 'Resources')
    self.initial_file = "index.html"
    
  def inject_compatibility_hacks(self):
    self.INJECTED_CODE += ('<script src="file://%s/yahoo_compatibility.js">' + 
               '</script>\n') % self.RES_DIR
  
  def inject_widget_object(self):
    self.INJECTED_CODE += ('<script src="file://%s/widget_object.js">' + 
               '</script>\n') % self.RES_DIR
  
  def inject_extra(self):
    # don't really need closebox, but the main jackfield code calls it
    self.INJECTED_CODE += (
     '''<script src="file://%s/closebox.js"></script>''' % (self.RES_DIR))
    
  def fix_html(self):
    pass
    

  def copy_to_temp_dir(self):
    """Create a temporary copy of the widget in the system temp dir
       by parsing the .kon file, making an HTML file out of it,
       and copying everything else in the widget into the same folder"""
    widgetname = os.path.split(self.original_widget_path)[1]
    tdir = tempfile.mkdtemp(prefix=widgetname,dir=self.root_temporary_dir)
    self.widget_path = os.path.join(tdir,'widget')
    try:
      zipf = zipfile.ZipFile(self.original_widget_path, 'r')
      iszipfile = True
    except:
      iszipfile = False

    if iszipfile:
      self.copy_zipfile_to_temp_dir(zipf)
    else:
      self.copy_folder_to_temp_dir()
      
    self.convert_kon_to_html()
      
  def copy_zipfile_to_temp_dir(self, zipf):
    # yahoo widgets have a folder in the root named after the widget,
    # then one inside that called Contents, then the actual widget data
    # So find out what the top two folders are, then strip them out
    topfolder = [x for x in zipf.namelist() 
                 if x.lower().find('/contents/') == -1][0]
    print topfolder
    removestring = "ahaha"
    if topfolder+'Contents/' in zipf.namelist():
      removestring = topfolder+'Contents/'
    if topfolder+'contents/' in zipf.namelist():
      removestring = topfolder+'contents/'
    #print "REMOVESTRING",removestring
    #print "searching for",topfolder+'/Contents/'
    #print "namelist",zipf.namelist()
    #print "found it?",(topfolder+'/Contents/') in zipf.namelist()
    # create folders
    for name in [x for x in zipf.namelist() if x.endswith('/')]:
      strippedname = name.replace(removestring,'')
      ndir = os.path.join(self.widget_path,strippedname)
      if not os.path.isdir(ndir): os.makedirs(ndir)
    for name in [x for x in zipf.namelist() if not x.endswith('/')]:
      strippedname = name.replace(removestring,'')
      fp = open(os.path.join(self.widget_path,strippedname), 'wb')
      fp.write(zipf.read(name))
      fp.close()
    #print "copied zipfile yahoo widget to",self.widget_path

  def copy_folder_to_temp_dir(self):
    print "NOT IMPLEMENTED: BOOM"

  def convert_kon_to_html(self):
    kon = [x for x in os.listdir(self.widget_path) if x.endswith('.kon')][0]
    k = KonParser(kon)
    fp = open(os.path.join(self.widget_path, "index.html"), "w")
    fp.write(k.tohtml())
    fp.close()

class KonParser:
  def __init__(self, kon):
    self.kon = kon
    
  def tohtml(self):
    return "<html><body>YAHOO</body></html>"
