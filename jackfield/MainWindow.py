import gtk, gtkmozembed, os, tempfile, gconf, urllib, Image, atexit
import shutil, ConfigParser
import WidgetBase, IconBar

import jackfield

class MainWindow:
  def __init__(self):
    # Load configuration data
    self.__config = ConfigParser.ConfigParser()
    if not os.path.exists(os.path.expanduser(jackfield.JACKFIELD_USER_DIR)):
      os.mkdir(os.path.expanduser(jackfield.JACKFIELD_USER_DIR))
    
    configdir = os.path.expanduser(jackfield.JACKFIELD_USER_DIR)
    self.__configfile = os.path.join(configdir,"Settings")
    self.__config.read([self.__configfile])
    
    self.window = gtk.Window()
    self.window.set_decorated(False)
    
    self.HTMLUIDIR = os.path.join(jackfield.DATADIR, jackfield.PACKAGE)

    # Create an empty mozilla profile folder to put our prefs.js in
    # It seems that Moz doesn't actually need the contents of the profile
    # folder to exist, so we just make an empty one and put our prefs.js
    # in it, and it all works
    MOZ_PROFILE = os.path.join(configdir,"private-mozilla-profile")
    try:
      os.makedirs(MOZ_PROFILE)
    except:
      pass
    # the mozilla profile folder will now exist; copy our prefs.js into it
    print os.path.join(MOZ_PROFILE, "prefs.js")
    print os.path.join(self.HTMLUIDIR,"prefs.js")
    print os.path.exists(os.path.join(self.HTMLUIDIR,"prefs.js")) == True

    shutil.copyfile(os.path.join(self.HTMLUIDIR,"prefs.js"),
                    os.path.join(MOZ_PROFILE, "prefs.js"))

    # and touch prefs.js, which seems to be required
    os.utime(os.path.join(MOZ_PROFILE, "prefs.js"), None)
    
    gtkmozembed.set_profile_path(configdir, "private-mozilla-profile")
    self.moz = gtkmozembed.MozEmbed()

    self.window.add(self.moz)
    self.moz.connect("title",self.__get_data_from_html)
    self.moz.show()
    window_size = int(gtk.gdk.screen_width()/1.6), int(gtk.gdk.screen_height()/1.6)
    window_size = int(gtk.gdk.screen_width()), int(gtk.gdk.screen_height())
    self.moz.set_size_request(window_size[0], window_size[1])
    self.window.show_all()

    BASE_HTML_DOCUMENT = os.path.join(self.HTMLUIDIR,"jackfield_base.html")
    print BASE_HTML_DOCUMENT
    fp = open(BASE_HTML_DOCUMENT)
    base_html = fp.read()
    fp.close()
    self.moz.open_stream("file://" + BASE_HTML_DOCUMENT,'text/html')
    self.__send_data_to_html(base_html)
    
    # Create a temporary directory in which all our temporary stuff happens
    self.TEMPDIR = tempfile.mkdtemp()
    # and remove it when we quit
    atexit.register(self.__remove_tempdir)
    
    try:
      use_background = self.__config.getboolean("Jackfield","use_background")
    except:
      use_background = True

    if use_background:
      # set background to an opaque screenshot
      # Use ImageMagick's import command if you have it
      if os.path.isfile("/usr/bin/import"):
        tmpimg = tempfile.mkstemp()[1]
        os.system('import -window root %s' % tmpimg)
        im1 = Image.open(tmpimg)
        USED_IMAGEMAGICK = True
      else:
        import GetXScreenshot
        im1 = GetXScreenshot.GetXScreenshot(window_size[0], window_size[1])
        USED_IMAGEMAGICK = False
      # composite the background with the Python imaging module to
      # get a semi-transparent version of it
      im1 = im1.resize(window_size,Image.ANTIALIAS).convert('RGBA')
      im2 = Image.new('RGBA', window_size, 'black')
      mask = Image.new('L', window_size, '#333333')
      im3 = Image.composite(im1,im2,mask)
      oshandle, tmpimagefilename = tempfile.mkstemp('.png', 'background', 
                                          self.TEMPDIR, False)
      im3.save(tmpimagefilename)
      del(im1)
      del(im2)
      del(im3)
      del(mask)
      if USED_IMAGEMAGICK:
        os.remove(tmpimg)
        
      self.__send_js_to_html("JackfieldBase.set_background('file://%s');" % 
              tmpimagefilename)
    
    # create IconBar
    self.IconBar = IconBar.IconBar(os.path.join(jackfield.PYTHONDIR, jackfield.PACKAGE))
    self.populate_iconbar()
    
    # Load previous widgets
    for widgetblock in self.__config.sections():
      if widgetblock == "Jackfield": continue
      this_block_options = self.__config.options(widgetblock)
      if "widgetpath" in this_block_options:
        widgetpath = self.__config.get(widgetblock, "widgetpath")
        if os.path.exists(widgetpath):
          xposition = -1
          yposition = -1
          if "xposition" in this_block_options: 
            xposition = self.__config.get(widgetblock, "xposition")
          if "yposition" in this_block_options: 
            yposition = self.__config.get(widgetblock, "yposition")
          self.add_widget(urllib.quote(widgetpath), widgetblock, xposition, yposition)
          continue
          
      # if we get to here, there's something wrong with this 
      # widgetblock (it doesn't have a widgetpath, or the path doesn't
      # exist). Delete it.
      self.__config.remove_section(widgetblock)

  def __remove_tempdir(self):
    print "REMOVING TEMP DIR"
    shutil.rmtree(self.TEMPDIR, True)
    
  def __send_js_to_html(self, js):
     self.__send_data_to_html(
       '<script type="text/javascript">\n%s\n</script>\n' % js)
    
  def __send_data_to_html(self, data):
    self.moz.append_data(data, long(len(data)))
    
  def __get_data_from_html(self, moz):
    title = moz.get_title()
    if title.find(":::") == -1:
      return # we didn't set the title, the widget must have done 
    data = title.split(":::")
    function_name = data[0]
    args = data[1:] or []
    try:
      function = getattr(self, function_name)
    except:
      print "Called non-existent function", function_name
      return # called a non-existent function
    retval = function(*args)
    if retval is not None:
      self.__send_js_to_html("JackfieldBase.SERVER_RETURN_DATA = '%s';" % retval.replace("'","\\'"))
    
  def add_widget(self, widget_path_escaped, iframe_name=None,
                 xposition=-1, yposition=-1):
    widget_path = urllib.unquote(widget_path_escaped)
    widget_engine = self.IconBar.get_widget_engine(widget_path)    
    widget = widget_engine.Widget(widget_path, self.TEMPDIR, 
                              self.HTMLUIDIR, iframe_name, 
                              xposition, yposition)
    widget.run(callback_function=self.display_widget)
    
  def display_widget(self, original_widget_path, widget_path, 
      iframe_name, xposition, yposition):
    "Called back when there's a widget ready to display"
    self.__send_js_to_html("JackfieldBase.load_widget('%s','%s', %s, %s);" % (
      urllib.quote(widget_path), iframe_name, xposition, yposition))
    # and save the widget in the config file so it gets reloaded
    if not self.__config.has_section(iframe_name):
      self.__config.add_section(iframe_name)
      self.__config.set(iframe_name, "widgetpath", original_widget_path)
      fp = open(self.__configfile,"w")
      self.__config.write(fp)
      fp.close()

  def populate_iconbar(self):
    add_widgets, delete_widgets = self.IconBar.get_changed_widgets()
    aw = [{
      "icon":urllib.quote(x["icon"]), 
      "path":urllib.quote(x["path"])} for x in add_widgets]
    dw = [x["path"] for x in delete_widgets]
    # take shameful advantage of JS and Python having the same list syntax
    js = "JackfieldBase.update_iconbar(%s,%s);" % (repr(aw), repr(dw))
    self.__send_js_to_html(js)
    
  # FUNCTIONS TO BE CALLED FROM A WIDGET
  def save_widget_location(self, iframe_name, x, y):
    self.__config.set(iframe_name, "xposition", x)
    self.__config.set(iframe_name, "yposition", y)
    fp = open(self.__configfile,"w")
    self.__config.write(fp)
    fp.close()

  def set_widget_data(self, iframe_name, key, value):
    self.__config.set(iframe_name, key, value)
    fp = open(self.__configfile,"w")
    self.__config.write(fp)
    fp.close()
    
  def get_widget_data(self, iframe_name, key):
    try:
      ret = self.__config.get(iframe_name, key)
    except:
      ret = ""
    return ret

  def js_error(self, *args):
    print args


