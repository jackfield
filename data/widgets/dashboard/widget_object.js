
window.widget = {

  // Standard properties
  ondragstart: function() {},
  ondragstop: function() {},
  onhide: function() {},
  onremove: function() {},
  onshow: function() {},

  openApplication: function(bundleId) {
    // Not implemented
  },
  
  openURL: function(url) {
  },
  
  preferenceForKey: function(key) {
    return undefined;
  },
  
  prepareForTransition: function() {},
  performTransition: function() {},
  
  setCloseBoxOffset: function(x,y) {
    // Not implemented
  },
  
  setPreferenceForKey: function(preference,key) {
    // Not implemented
  },
  
  system: function(command,endHandler) {
    // Not implemented
  }
  
}

