Jackfield.CloseBox = {
  init: function() {
    Jackfield.CloseBox.closebox_element = document.createElement('img');
    Jackfield.CloseBox.closebox_element.src = Jackfield.ResourcesPath + '/active-close-button.png';
    Jackfield.CloseBox.closebox_element.id = 'jackfield_closebox';
    Jackfield.CloseBox.closebox_element.style.left = Jackfield.CloseBox.x + 'px';
    Jackfield.CloseBox.closebox_element.style.top = Jackfield.CloseBox.y + 'px';
    document.body.appendChild(Jackfield.CloseBox.closebox_element);
  },
  
  show_closebox: function() {
    Jackfield.CloseBox.closebox_element.style.display = 'block';
  },
  
  hide_closebox: function() {
    Jackfield.CloseBox.closebox_element.style.display = 'none';
  }
}
