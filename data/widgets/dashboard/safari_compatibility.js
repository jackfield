/* Safari compatibility library */

Jackfield.Compatibility = {
  init: function() { 
    Jackfield.Compatibility.hideScrolling();
    Jackfield.Compatibility.innerTextSupport();
    Jackfield.Compatibility.fromToElementSupport();
    Jackfield.Compatibility.caseInsensitiveGetElementById();
    Jackfield.Compatibility.makeXMLHttpRequestPrivileged();
  },
  hideScrolling: function() {
    document.body.style.overflow = 'hidden';
    document.documentElement.style.overflow = 'hidden';
  },
  
  innerTextSupport: function() {
    HTMLElement.prototype.__defineSetter__("innerText", 
      Jackfield.Compatibility.innerTextSetter);
  },
  innerTextSetter: function (sText) {
    this.innerHTML = sText;
  },
  
  fromToElementSupport: function() {
      Event.prototype.__defineGetter__("fromElement", function () {
       var node;
       if (this.type == "mouseover")
          node = this.relatedTarget;
       else if (this.type == "mouseout")
          node = this.target;
       else
          return null;
       var node = this.target;
       while (node.nodeType != 1) node = node.parentNode;
       return node;
    });

    Event.prototype.__defineGetter__("toElement", function () {
       var node; 
       if (this.type == "mouseout")
          node = this.relatedTarget;
       else if (this.type == "mouseover")
          node = this.target;
       else
          node = this.target;
       while (node.nodeType != 1) node = node.parentNode;
       return node;
    });
  },
  
  caseInsensitiveGetElementById: function() {
		document._getElementById = document.getElementById;
		document.getElementById = function(i) {
		  var e = document._getElementById(i);
		  if (e === null) {
		    // couldn't find an element with that ID
		    // try and find one case-insensitively, because Safari does
		    this_xpath = "//*[translate(@id, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') = '" + i.toUpperCase() + "']";
		    var matches = document.evaluate(this_xpath, document, null, 
		        XPathResult.FIRST_ORDERED_NODE_TYPE,null);
		    return matches.singleNodeValue;
		  } else {
		    return e;
		  }
		}  
	},
	
	makeXMLHttpRequestPrivileged: function() {
    XMLHttpRequest.prototype.__open = XMLHttpRequest.prototype.open;
    XMLHttpRequest.prototype.open = function(verb,url,boole) {
      /* only lasts for the duration of the function */
      netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect UniversalBrowserRead UniversalPreferencesRead");  
      this.__open(verb,url,boole);
    }	
  }
}

window.addEventListener("DOMContentLoaded", Jackfield.Compatibility.init, false);


