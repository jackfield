/* Safari compatibility library */

Jackfield.Compatibility = {
  init: function() { 
    Jackfield.Compatibility.allowDocumentAppend();
  },
  
  allowDocumentAppend: function() {
    document.appendChild = function(el) {
      document.body.appendChild(el);
    }
    document.removeChild = function(el) {
      document.body.removeChild(el);
    }
  }
}

window.addEventListener("DOMContentLoaded", Jackfield.Compatibility.init, false);

