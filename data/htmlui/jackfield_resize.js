Jackfield.checkSize = function () {
  al = document.getElementsByTagName('*');
  le = al.length;
  maxx = 0;
  maxy = 0;
  for (var i=0; i<le; i++) {
    el = al[i];
    xs = el.offsetLeft+el.offsetWidth;
    if (xs > maxx) maxx = xs;
    ys = el.offsetTop+el.offsetHeight;
    if (ys > maxy) maxy = ys;
  }
  xs += 30; ys += 30; // stay a bit bigger than the biggest
  parent.JackfieldBase.resize_iframe(Jackfield.iframe_name, maxx, maxy);
}

window.setInterval(Jackfield.checkSize, 100); 
