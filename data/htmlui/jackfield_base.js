JackfieldBase = {
  load_widget_request: function (path) {
    // The iconbar calls this when a widget is clicked, to request loading it
    JackfieldBase.send_to_jackfield_server("add_widget",path);
    return false;
  },

  send_to_jackfield_server: function() {
    // Generic way of calling a function on the back end. Pass
    // the name of the function you want to call as the first argument,
    // and params for that function as subsequent arguments.
    titlestr = "";
    for (var i=0; i<arguments.length; i++) {
      if (i>0) titlestr += ':::';
      titlestr += arguments[i].toString();
    }
    JackfieldBase.SERVER_RETURN_DATA = null;
    document.title = titlestr;
    // amazingly, if the server event that detects the title change sends new
    // script to be run by appending a script element, that script element
    // gets run here *before* we continue!
    return JackfieldBase.SERVER_RETURN_DATA; 
  },

  load_widget: function (path, iframe_name, x, y) {
    // Called by the back end to *actually* load a widget
    var ifr = document.createElement('iframe');
    ifr.id = 'jackfield_'+iframe_name;
    ifr.src = 'file://'+path;
    document.body.appendChild(ifr);
    if (x == -1 || y == -1) {
      // position it ourselves
    } else {
      JackfieldBase.drag_iframe(iframe_name, x, y);
    }
  },

  resize_iframe: function(iframe_name, x, y) {
    // called by Jackfield.resize when the widget resizes
    ifr = document.getElementById('jackfield_'+iframe_name);
    ifr.style.width = x + 'px';
    ifr.style.height = y + 'px';  
  },

  drag_iframe: function(iframe_name, movetox, movetoy) {
    // Called by Jackfield.drag when the user drags a widget
    ifr = document.getElementById('jackfield_'+iframe_name);
    ifr.style.left = movetox + 'px';
    ifr.style.top = movetoy + 'px'; 
  },
  
  finish_dragging_iframe: function(iframe_name, movetox, movetoy) {
    // Called by Jackfield.drag when the user finished dragging a widget
    ifr = document.getElementById('jackfield_'+iframe_name);
    JackfieldBase.send_to_jackfield_server("save_widget_location",
       iframe_name, movetox, movetoy);
  },
  
  iframe_mousedown: function(iframe_name, xoffset, yoffset) {
    d = document.createElement('div');
    d.style.position = 'absolute';
    d.style.top = "0";
    d.style.left = "0";
    d.style.width = "100%";
    d.style.height = "100%";
    d.style.zIndex = "9999";
    var ifr = document.getElementById('jackfield_'+iframe_name);
    d.onmousemove = function(e) {
      ifr.style.left = (e.pageX - xoffset) + 'px';
      ifr.style.top = (e.pageY - yoffset) + 'px';
    }
    d.onmouseup = function(e) {
      JackfieldBase.finish_dragging_iframe(iframe_name, 
         e.pageX - xoffset, e.pageY - yoffset);
      document.body.removeChild(d);
      delete d;
    }
    document.body.appendChild(d);
  },
  
  set_background: function(bgfile) {
    document.body.style.backgroundImage = "url('"+bgfile+"')";
  },
  
  set_widget_data: function(iframe_name, key, value) {
    JackfieldBase.send_to_jackfield_server("set_widget_data",
           iframe_name, key, value);
  },
  
  get_widget_data: function(iframe_name, key) {
    return JackfieldBase.send_to_jackfield_server("get_widget_data",
           iframe_name, key);
  },
  
  update_iconbar: function(added_widgets, deleted_widgets) {
    // added_widgets is a list of {"icon":icon,"path":path} directories
    // deleted widgets is a list of paths
    var iconbar = document.getElementById('iconbar');

    for (var i=0; i<added_widgets.length; i++) {
      var li = document.createElement('li');
      li.className = "widget_icon";
      li.onclick = function(e) { 
        JackfieldBase.load_widget_request(e.target.widget_path);
      }
      li.style.backgroundImage = 'url(file://' + added_widgets[i]["icon"] + ')';
      li.widget_path = added_widgets[i]["path"];
      iconbar.appendChild(li);
    }
    
    lis = iconbar.getElementsByTagName('li');
    for (var i=0; i<deleted_widgets.length; i++) {
      for (var j=lis.length-1;j>=0;j--) {
        if (lis[j].widget_path == deleted_widgets[i]) {
          iconbar.removeChild(lis[j]);
        }
      }
    }
    
    JackfieldBase.iconbar_update_page_counts();
  },
  
  iconbar_update_page_counts: function() {
    // update the page counts
    iconbar = document.getElementById('iconbar');
    lis = iconbar.getElementsByTagName('li');
    rows_of_icons = {}
    rows = 0;
    showing_row = 1;
    for (var i=0; i<lis.length; i++) {
      if (lis[i].className != 'widget_icon') continue;
      ot = lis[i].offsetTop;
      if (ot < 50 && ot >= 0) ot = 0; // copes with how they may not all be
                           // initialised when this code runs
      if (ot in rows_of_icons) {
        rows_of_icons[ot] += 1;
      } else {
        rows_of_icons[ot] = 1;
        rows++;
      }
    }
    mt = parseInt(iconbar.style.marginTop) || 0;
    showing_row = (mt / -85) + 1;
    document.getElementById('iconbar_pagetotal_left').innerHTML = rows.toString();
    document.getElementById('iconbar_pagetotal_right').innerHTML = rows.toString();
    document.getElementById('iconbar_page_left').innerHTML = showing_row.toString();
    document.getElementById('iconbar_page_right').innerHTML = showing_row.toString();
    
    document.getElementById('iconbar_left').style.display = 
      (showing_row > 1) ? "block" : "none";
    document.getElementById('iconbar_right').style.display = 
      (showing_row < rows) ? "block": "none";
  },
  
  iconbar_scroll_left: function() {
    // subtract 90px from iconbar.margin-top, add 90px to iconbar.height
    mt = parseInt(document.getElementById('iconbar').style.marginTop) || 0;
    h = parseInt(document.getElementById('iconbar').style.height) || 80;
    setTimeout(function() { 
      JackfieldBase.iconbar_scroll_step(0, 5, 85, mt, h); 
    }, 10);
  },
  iconbar_scroll_right: function() {
    // add 90px to iconbar.margin-top, subtract 90px from iconbar.height
    mt = parseInt(document.getElementById('iconbar').style.marginTop) || 0;
    h = parseInt(document.getElementById('iconbar').style.height) || 80;
    setTimeout(function() { 
      JackfieldBase.iconbar_scroll_step(0, -5, -85, mt, h); 
    }, 10);
  },
  
  iconbar_scroll_step: function(steptotal, stepsize, stepend, 
                                startmargin, startheight) {
    steptotal += stepsize;
    iconbar = document.getElementById('iconbar');
    iconbar.style.marginTop = (startmargin + steptotal) + 'px';
    iconbar.style.height = (startheight - steptotal) + 'px';
    if (steptotal == stepend) {
      JackfieldBase.iconbar_update_page_counts();
    } else {
      setTimeout(function() { 
        JackfieldBase.iconbar_scroll_step(steptotal, stepsize, stepend, 
                                          startmargin, startheight); 
      }, 10);
    }
  },
  
  toggle_iconbar: function() {
    JackfieldBase.send_to_jackfield_server("populate_iconbar");
    iconbar = document.getElementById('iconbar');
    if (iconbar.offsetLeft == 0) {
      // is showing
      iconbar.style.marginLeft = "0%";
      setTimeout(JackfieldBase.iconbar_hide,10);
    } else {
      iconbar.style.marginLeft = "100%";
      setTimeout(JackfieldBase.iconbar_show,10);
    }
  },
  
  iconbar_hide: function() {
    iconbar = document.getElementById('iconbar');
    ml = parseInt(iconbar.style.marginLeft) + 5;
    iconbar.style.marginLeft = ml + "%";
    if (ml < 100) {
      setTimeout(JackfieldBase.iconbar_hide,10);
    } else {
      JackfieldBase.iconbar_update_page_counts();
      // hide all close boxes
      ifrs = document.getElementsByTagName('iframe');
      for (var i=0; i<ifrs.length; i++) {
        ifrs[i].contentWindow.Jackfield.CloseBox.hide_closebox();
      }
    }
  },
  iconbar_show: function() {
    iconbar = document.getElementById('iconbar');
    ml = parseInt(iconbar.style.marginLeft) - 5;
    iconbar.style.marginLeft = ml + "%";
    if (ml >0) {
      setTimeout(JackfieldBase.iconbar_show,10);
    } else {
      JackfieldBase.iconbar_update_page_counts();
      // show all close boxes
      ifrs = document.getElementsByTagName('iframe');
      for (var i=0; i<ifrs.length; i++) {
        ifrs[i].contentWindow.Jackfield.CloseBox.show_closebox();
      }
    }

  }
}


if (navigator.userAgent.indexOf('en-GB') != -1) {
  // real firefox :-) bodge!
  window.onload = function() {JackfieldBase.update_iconbar([
{"icon":"/home/aquarius/Projects/jackfield/Widgets/Not%20Working/WeeklyComicShippingList.wdgt/Icon.png","path":""},
{"icon":"/home/aquarius/Projects/jackfield/Widgets/Not%20Working/Sol.wdgt/Icon.png","path":""},
{"icon":"/home/aquarius/Projects/jackfield/Widgets/Not%20Working/BBC%20Radio.wdgt/Icon.png","path":""},
{"icon":"/home/aquarius/Projects/jackfield/Widgets/Not%20Working/romantest.wdgt/Icon.png","path":""},
{"icon":"/home/aquarius/Projects/jackfield/Widgets/Not%20Working/JDClock.wdgt/Icon.png","path":""},
{"icon":"/home/aquarius/Projects/jackfield/Widgets/Not%20Working/Deography.wdgt/Icon.png","path":""},
{"icon":"/home/aquarius/Projects/jackfield/Widgets/Not%20Working/Whisky.wdgt/Icon.png","path":""},
{"icon":"/home/aquarius/Projects/jackfield/Widgets/Liverpool%20FC.wdgt/Icon.png","path":""},
{"icon":"/home/aquarius/Projects/jackfield/Widgets/Chat%20Acronyms.wdgt/Icon.png","path":""},
{"icon":"/home/aquarius/Projects/jackfield/Widgets/GooglePageRank.wdgt/Icon.png","path":""}
  ],[]);}
}

myobj = {
  QueryInterface: function() {
    return this;
  },
  observe: function(nsiConsoleMessage) {
    try {
      JackfieldBase.send_to_jackfield_server("js_error", nsiConsoleMessage.message);
    } catch(e) {
    }
  }
}
netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect UniversalBrowserRead UniversalPreferencesRead CapabilityPreferencesAccess");
var srv = Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService);
srv.registerListener(myobj);

